var app = require('express').Router();

exports = module.exports = function(di) {
  app
    .route('/')
    .all(function(req, res) {
      return res.render('index');
    });

  return app;
};