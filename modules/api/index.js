var app = require('express').Router();

exports = module.exports = function(di) {
  app
    .route('/')
    .all(function(req, res) {
      res.json({
        status: 'OK',
        meta: {
          available:['/', '/events']
        }
      })
    });

  app.use('/events', require('./events').resolve());

  return app;
};