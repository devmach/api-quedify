var routes = {};

routes.search = function(Events) {
  return function(req, res) {
    /**
     * In production we probably want to have pagination and
     * leverage streams if possible...
     *
     * Also, we used regexp butr in production we should have
     * use text search.
     */
    var query = new RegExp('^' + (req.query.title || ''), 'i');
    Events
      .find({title: query})
      .limit(10)
      .exec(function(error, docs) {
        if (error) {
          return res
            .status(500)
            .json({error: true, meta: error})
        }

        return res.json({status: 'OK', data: docs});
      });
  };
};

/**
 * To test with cURL:
 *
 * curl -v -d '{"title":"Sample Event"}' -H "Content-Type: application/json" http://localhost:5000/api/events
 */

routes.create = function(Events) {
  return function(req, res) {
    Events
      .create(req.body, function(error, doc) {
        if (error) {
          return res
            .status(500)
            .json({error: true, meta: error})
        }

        return res.json({status: 'OK', data: [doc]});
      });
  };
};

/**
 * To test with cURL:
 *
 * curl -v -H "Content-Type: application/json" http://localhost:5000/api/events/:id
 */

routes.find = function(Events) {
  return function(req, res, next) {
    Events
      .findOne({_id: req.params.id}, function(error, doc) {
        if (error) {
          return res
            .status(500)
            .json({error: true, meta: error})
        }

        if (!doc) {
          return res
            .status(404)
            .json({error: true, meta: 'Not found!'})
        }
        req.event = doc;
        next();
      });
  };
};

routes.get = function(Events) {
  return function(req, res) {
    return res.json({status: 'OK', data: [req.event]});
  };
};

/**
 * To test with cURL:
 *
 * curl -v -X PUT -d '{"title":"Sample Event"}' -H "Content-Type: application/json" http://localhost:5000/api/events/:id
 */

routes.update = function(Events) {
  return function(req, res) {
    var update = req.body;
    delete update._id;

    Events
      .update({_id: req.event._id}, update, {multi: false}, function(error) {
        if (error) {
          return res
            .status(500)
            .json({error: true, meta: error})
        }

        return res.json({status: 'OK', data: [update]});
      });
  };
};

/**
 * To test with cURL:
 *
 * curl -v -X DELETE -H "Content-Type: application/json" http://localhost:5000/api/events/55cc18020f14ae1c0cc59e03
 */

routes.remove = function(Events) {
  return function(req, res) {
    req.event.remove(function(error) {
        if (error) {
          return res
            .status(500)
            .json({error: true, meta: error})
        }

        return res.json({status: 'OK', data: [req.event]});
    });
  };
};


exports = module.exports = routes;