var app = require('express').Router();
var routes = require('./routes.js');

exports = module.exports = function(di) {
  app
    .route('/')
    .get(routes.search.resolve())
    .post(routes.create.resolve());

  app
    .route('/:id')
    .all(routes.find.resolve())
    .get(routes.get.resolve())
    .put(routes.update.resolve())
    .delete(routes.remove.resolve());

 return app;
};