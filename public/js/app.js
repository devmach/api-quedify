/**
 * SOME IMPORTANT NOTES
 *
 * + In a production, this file would be splitted different files, each directive,
 *   controller etc would have it its own file and they would be added each other,
 *   minified and processed (eg if you use ES6 transpiled with babeljs etc).
 *
 * + To be able to resue the code in different places and being more modular, we have
 *   bunch of directives for each action (add,edit,search an event). We can say that
 *   they work like small components
 *
 * + UI tests IGNORED in this example. In production we probably want to test:
 *
 *   - If controllers and directives are working as exected ( via unit tests )
 *   - If the ui works ok in diffrent browsers (using saucelabs, selenium etc)
 *   - If the ui and api works as expectecd in diffrent browsers (using saucelabs,
 *     selenium etc)
 *
 * + To be honest, we (ok me...Aydin...) havent spend time to make this example look
 *   pretty. Main intention was making it functional.
 *
 * + We used `alert` and `confirm` to report the result of the action or ask a permission
 *   In production, using `modals` would be better
 */


var app = angular.module('app', ['ngRoute', 'ui.bootstrap.datetimepicker']);

app.
  config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/templates/index',
        controller: 'IndexController'
      })
      .when('/events/:id', {
        templateUrl: '/templates/event_show',
        controller: 'EventShowController',
        resolve : {
          /**
           * We want to have the `event` before angular renders the ui
           * one catch is, if api can not find the event, this promise
           * will fail. We didn't cared this situation but in production
           * we don't have this luxery.
           */
          Event : function($route, $http) {
            return $http.get('/api/events/' + $route.current.params.id);
          }
        }
      })
      .otherwise({
        redirectTo: '/'
      });
  });

app
  .directive('eventNew', function() {
    return {
      templateUrl: '/templates/directives/event_new',
      controller: function($scope, $http) {
        $scope.model = {};

        $scope.resetModel = function() {
          $scope.model = {
            title: "",
            participants: [],
            from: new Date(),
            to: new Date()
          };

          $scope.newParticipant = "";
        };

        $scope.addNewParticipant = function(person) {
          if(person.trim() === "") {
            return;
          }

          $scope.model.participants.push(person);
          $scope.newParticipant = "";
        };

        $scope.removeParticipant = function(index) {
          $scope.model.participants.splice(index, 1);
        };

        $scope.save = function() {
          $http
            .post('/api/events', $scope.model)
            .then(function(data){
              alert('Saved!');
              $scope.resetModel();
            }, function(data) {
              alert('Server error!');
            });
        };

        $scope.resetModel();
      }
    };
  })
  .directive('eventSearch', function() {
    return {
      templateUrl: '/templates/directives/event_search',
      controller: function($scope, $http) {
        $scope.title = "";
        $scope.events = [];

        $scope.search = function() {
          $http
            .get('/api/events', {params: {title: $scope.title}})
            .then(function(data) {
              $scope.events = data.data.data;
            }, function() {
              alert('Server Error!');
            });
        };

        $scope.deleteEvent = function(index) {
          if( !confirm('Do you want to delete event: ' + $scope.events[index].title)) {
            return;
          }

          $http.
            delete('/api/events/' + $scope.events[index]._id)
            .then(function(data) {
              alert('Event deleted!');
              $scope.events.splice(index, 1);
            }, function() {
              alert('Server Error!');
            });
        }
      }
    };
  })
  .directive('eventEdit', function() {
    return {
      templateUrl: '/templates/directives/event_edit',
      scope: {
        event: "="
      },
      controller: function($scope, $http) {
        $scope.resetModel = function() {
          $scope.model = angular.copy($scope.event);
          $scope.newParticipant = "";
        };

        $scope.addNewParticipant = function(person) {
          if(person.trim() === "") {
            return;
          };

          $scope.model.participants.push(person);
          $scope.newParticipant = "";
        };

        $scope.removeParticipant = function(index) {
          $scope.model.participants.splice(index, 1);
        };

        $scope.save = function() {
          $http
            .put('/api/events/' + $scope.event._id, $scope.model)
            .then(function(data){
              alert('Saved!');
              $scope.event = angular.copy($scope.model);
            }, function(data) {
              alert('Server error!');
            });
        };

        $scope.resetModel();
      }
    };
  })
  .controller('IndexController', function($scope) {
    $scope.activePanel = 'SEARCH'
  })
  .controller('EventShowController', function($scope, Event) {
    $scope.event = Event.data.data[0];
  })
