var fs = require('fs');
var yaml = require('js-yaml');
var extend = require('util')._extend;

var config = function(defaults, configFile) {
  try {
    var defaultConfig = yaml.safeLoad(fs.readFileSync(defaults, 'utf8'))
    extend(defaultConfig, yaml.safeLoad(fs.readFileSync(configFile, 'utf8')));
  } catch (e) {
    /* ... */
  }

  return defaultConfig;
};

module.exports = config;