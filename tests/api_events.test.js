var assert = require('assert');
var superagent = require('superagent');

/**
 * Setup from index.js In production this would be more organized....
 */

var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var betterDi = require('better-di')(null, {extendFn: true});
var config = require('configHelper')('./config.defaults.yaml', './config.test.yaml');

betterDi.register('di', betterDi);
betterDi.register('config', config);
betterDi.register('db', mongoose.createConnection(config.database.uri));
betterDi.register('models', require('models').resolve());


describe('/api/events api test', function() {
  var app;
  var server;
  var BASE_URL = 'http://localhost:3001/api/';

  beforeEach(function(done) {
    app = express();

    app
      .use(bodyParser.urlencoded({ extended: false }))
      .use(bodyParser.json())
      .use('/api', require('modules/api').resolve());

    betterDi.get('Events').remove({}, function() {
      server = app.listen(3001, function() {
        done();
      });
    });
  });

  afterEach(function() {
    server.close();
  });

  it('There should be no events in the beginning', function(done) {
    superagent
      .get(BASE_URL + '/events')
      .end(function(error, res) {

        assert.ok(res.ok);
        assert.equal(200, res.status);
        assert.equal(0, res.body.data.length);

        done();
      });
  });

  it('Create an event', function(done) {
    superagent
      .post(BASE_URL + '/events')
      .send({title: 'Test title'})
      .end(function(error, res) {
        assert.ok(res.ok);
        assert.equal(200, res.status);
        assert.equal(1, res.body.data.length);
        assert.equal('Test title', res.body.data[0].title);

        done();
      });
  });

  it('Get an event', function(done) {
    betterDi
      .get('Events')
      .create({_id:'0000000000000000DEADBEEF', title:"Test Title - 0000000000000000DEADBEEF"}, function() {
        superagent
          .get(BASE_URL + '/events/0000000000000000DEADBEEF')
          .end(function(error, res) {
            assert.ok(res.ok);
            assert.equal(200, res.status);
            assert.equal(1, res.body.data.length);
            assert.equal('Test Title - 0000000000000000DEADBEEF', res.body.data[0].title);

            done();
          });
      });
  });

  it('Update an event', function(done) {
    betterDi
      .get('Events')
      .create({_id:'0000000000000000DEADBEEF', title:"Test Title - 0000000000000000DEADBEEF"}, function() {
        superagent
          .put(BASE_URL + '/events/0000000000000000DEADBEEF')
          .send({title: 'Test Title - 00000000000000000BADCAFE'})
          .end(function(error, res) {
            assert.ok(res.ok);
            assert.equal(200, res.status);
            assert.equal(1, res.body.data.length);
            assert.equal('Test Title - 00000000000000000BADCAFE', res.body.data[0].title);


            /* Promises or something like async module would be better here....*/
            superagent
              .get(BASE_URL + '/events/0000000000000000DEADBEEF')
              .end(function(error, res) {
                assert.ok(res.ok);
                assert.equal(200, res.status);
                assert.equal(1, res.body.data.length);
                assert.equal('Test Title - 00000000000000000BADCAFE', res.body.data[0].title);
                done();
              });
          });
      });
  });

  it('Delete an event', function(done) {
    betterDi
      .get('Events')
      .create({_id:'0000000000000000DEADBEEF', title:"Test Title - 0000000000000000DEADBEEF"}, function() {
        superagent
          .del(BASE_URL + '/events/0000000000000000DEADBEEF')
          .end(function(error, res) {
            assert.ok(res.ok);
            assert.equal(200, res.status);
            assert.equal(1, res.body.data.length);
            assert.equal('Test Title - 0000000000000000DEADBEEF', res.body.data[0].title);


            /* Promises or something like async module would be better here....*/
            superagent
              .get(BASE_URL + '/events/0000000000000000DEADBEEF')
              .end(function(error, res) {
                assert.ok(res. notFound);
                assert.equal(404, res.status);

                done();
              });
          });
      });
  });

});
