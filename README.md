# README #
This an example API server with nodejs + mongodb + mongoose + angular

### How to install dependencies and start the server ###

```
$ npm install
$ npm run server
```

### How to run API tests ###

```
$ npm run test
```

### How to use different database server than local ###

Please update the mongodb uri in `config.defaults.yaml`

``` yaml
database:
  uri: mongodb://localhost/database
```