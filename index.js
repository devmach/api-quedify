/**
 * Copyright (C) Aydin Ulfer - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 *
 */

var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var betterDi = require('better-di')(null, {extendFn: true});
var config = require('./configHelper.js')('./config.defaults.yaml', './config.development.yaml');

var app = express();

betterDi.register('di', betterDi);
betterDi.register('config', config);
betterDi.register('db', mongoose.createConnection(config.database.uri));

/**
 * We actually don't need to register models, since they are automatically registered to di
 * but it might be handy to have list of available models.
 */
betterDi.register('models', require('./models').resolve());


/**
 * Some standard defaults for the express
 */
app
  .disable('x-powered-by')
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'jade')
  .use(bodyParser.urlencoded({ extended: false }))
  .use(bodyParser.json())
  .use(express.static(path.join(__dirname, 'public')));

app
  .use('/', require('./modules/ui').resolve())
  .use('/templates', function(req, res) {
    var template = req.path.substring(1).replace(/[^a-z0-9_\/\-]/gi, '');
    res.render('templates/' + template);
  })
  .use('/api', require('./modules/api').resolve());

server = app.listen(process.env.PORT || config.server.port || 3000, function() {
  console.log(
    'Listening on %s:%s',
    server.address().address,
    server.address().port
  );
});
