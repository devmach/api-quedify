var mongoose = require('mongoose');

 var schema = new mongoose.Schema({
 "title": {type: String, required: true},
 "from": Date,
 "to": Date,
 "location": String,
 "description": String,
 "participants": [String]
}); 

schema.index({"title": "text"})

exports = module.exports = schema;