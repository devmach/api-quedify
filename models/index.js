exports = module.exports = function(di) {
  var EventSchema = require('./Events.js');
  var Events = di.get('db').model('events', EventSchema);
  di.register('Events', Events);

  /**
   * Heads up quedify people: this might be a bad idea
   * on production env...
   */ 
  Events.ensureIndexes(function(){ /* ... */});

  return ['Events'];
};